######################################################
# This is a Dockerfile example, not for this project #
######################################################


# This is your Editor pane. Write the Dockerfile here and
# use the command line to execute commands

#Define a base image
FROM nginx:1.11-alpine

#Copy index.html in this path inside the image
COPY index.html /usr/share/nginx/html/index.html

#Exposing port 80
EXPOSE 80

#Launch the application
CMD ["nginx", "-g", "daemon off;"]





##### Several commands for docker

# See docker images in the computer
# $ docker images

# Build a new image
# $ docker build -t my-nginx-image:latest .

# Run a docker image
# $ docker run -d -p 80:80 my-nginx-image

# See which images are executing
# $ docker ps

# Stop image execution
# $ docker stop admiring_haibt