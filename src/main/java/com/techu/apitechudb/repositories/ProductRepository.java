package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

// Te ayuda a abstraer todas las operaciones

@Repository
public interface ProductRepository extends MongoRepository<ProductModel, String> {

}
