package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderby", required = false) String orderBy) {
        System.out.println("getUsers in UserController");
        System.out.println("The value $orderby is " +orderBy);

        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById in UserController");
        System.out.println("The user to get is " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "User not found",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("addUser in UserController");
        System.out.println("The user to create is " + user.toString());

        return new ResponseEntity<>(this.userService.addUser(user),
                HttpStatus.CREATED);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user,
                                                @PathVariable String id){
        System.out.println("updateUser in UserController");
        System.out.println("The id user to update is " + id);
        System.out.println("The fields to update is " + user.toString());

        Optional<UserModel> userToUpdate = userService.findById(id);

        if (userToUpdate.isPresent()) {
            System.out.println("User to update found!, updating...");
            this.userService.update(user);
        }

        return new ResponseEntity<>(user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser in UserController");
        System.out.println("The id user to delete is " + id);

        boolean deletedUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "User deleted!!" : "User to delete not found...",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


}








