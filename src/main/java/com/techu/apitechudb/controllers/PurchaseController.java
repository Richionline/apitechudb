package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getPurchases in PurchaseController");

        return new ResponseEntity<>(this.purchaseService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/purchases/{id}")
    public ResponseEntity<Object> getPurchaseById(@PathVariable String id) {
        System.out.println("getPurchaseById in PurchaseController");
        System.out.println("The purchase to get is " + id);

        Optional<PurchaseModel> result = purchaseService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Purchase not found",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/purchases")
    public ResponseEntity<Object> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase in PurchaseController");
        System.out.println("The purchase to create is " + purchase.toString());

        PurchaseServiceResponse result = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(result, result.getResponseHttpStatusCode());
    }


}
