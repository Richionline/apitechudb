package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2") // Adding this path to all the urls
public class ProductController {

    @Autowired // Tiene que ver con el inyector de dependencias, evitara tener que instanciar
    ProductService productService;


    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts in ProductController");

        return new ResponseEntity<>(this.productService.findAll(), HttpStatus.OK);
    }

    //Ponemos que devuelve Object porque todos heredan de Object y no sabemos si devolvera un ProductModel o un String
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("getProductById in ProductController");
        System.out.println("The product to get is " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Product not found",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("addProduct in ProductController");
        System.out.println("The product to create is " + product.toString());

        return new ResponseEntity<>(this.productService.addProduct(product), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct in ProductController");
        System.out.println("The id product to update is " + id);
        System.out.println("The fields to update is " + product.toString());

        Optional<ProductModel> productToUpdate = this.productService.findById(id);

        if (productToUpdate.isPresent()) {
            System.out.println("Product to update found!, updating...");
            this.productService.update(product);
        }

        return new ResponseEntity<>(product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct in ProductController");
        System.out.println("The id product to delete is " + id);

        // La comprobacion de existencia en update lo comprabamos en el controller pero aqui lo hacemos en el Servicio
        boolean deletedProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deletedProduct ? "Product deleted!!" : "Product to delete not found...",
                deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
