package com.techu.apitechudb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// El flujo de componentes es de la capa más interna a más externa ---> Model > Repository > Service > Controller

@SpringBootApplication
public class ApitechudbApplication {

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);
	}

}
