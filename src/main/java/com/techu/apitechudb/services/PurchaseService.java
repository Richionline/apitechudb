package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;


    public List<PurchaseModel> findAll() {
        System.out.println("findAll in PurchaseService");

        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById in PurchaseService");

        return this.purchaseRepository.findById(id);
    }

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase in PurchaseService");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if (!this.userService.findById(purchase.getUserId()).isPresent()) {
            System.out.println("User ID purchase not found");

            result.setMsg("User ID purchase not found");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.purchaseRepository.findById(purchase.getId()).isPresent()) {
            System.out.println("Purchase ID already exists");

            result.setMsg("Purchase ID already exists");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (!this.productService.findById(purchaseItem.getKey()).isPresent()) {
                System.out.println("The product ID " + purchaseItem.getKey()
                        + " not found in the system");

                result.setMsg("The product ID " + purchaseItem.getKey()
                        + " not found in the system");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                System.out.println("Adding the value of " + purchaseItem.getValue()
                        + " product units to the total");

                amount += (this.productService.findById(purchaseItem.getKey()).get().getPrice()
                                * purchaseItem.getValue());
            }
        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Purchase added successfully with total price " + purchase.getAmount() + "!");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;
    }
}
