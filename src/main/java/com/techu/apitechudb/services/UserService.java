package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public List<UserModel> findAll(String orderBy) {
        System.out.println("findAll in UserService");

        List<UserModel> result;

        if ((orderBy != null) && (orderBy.toUpperCase().matches("ASC|DESC"))) {
            System.out.println("Requesting ordered");
            if (orderBy.equalsIgnoreCase("ASC")) {
                System.out.println("Sorting in mode ASC...");
                result = this.userRepository.findAll(Sort.by(Sort.Direction.ASC,"age"));
            } else {
                System.out.println("Sorting in mode DESC...");
                result = this.userRepository.findAll(Sort.by(Sort.Direction.DESC,"age"));
            }

        } else {
            System.out.println("Without sort users...");
            result = this.userRepository.findAll();
        }

        return result;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById in UserService");

        return this.userRepository.findById(id);
    }


    public UserModel addUser(UserModel user) {
        System.out.println("addUser in UserService");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("update in UserService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("delete in UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("User to delete found!! deleting...");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }

}
