package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll in ProductService");

        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("getProduct in ProductService");

        return this.productRepository.findById(id);
    }

    public ProductModel addProduct(ProductModel product) {
        System.out.println("addProduct in ProductService");

        return this.productRepository.save(product); //You could use .insert
    }

    public ProductModel update(ProductModel product) {
        System.out.println("update in ProductService");

        return this.productRepository.save(product);
    }

    public boolean delete(String id) {
        System.out.println("delete in ProductService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Product to delete found!! deleting...");
            this.productRepository.deleteById(id);

            result = true;
        }

        return result;
    }

}
